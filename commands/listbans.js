var MongoClient = require('mongodb').MongoClient;
var Discord = require("discord.js");
var url = "";
module.exports.run = (bot, msg, params) => {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log("Failed (" + err + ")");
            return false;
        }
    msg.reply("Please reply with a **UserID** or reply **View** to list all bans.");
    const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 10000, max: 1 });
    collector.on('collect', message => {
        if (message.content === "View" || message.content === "view") {
            var mysort = { _id: 1 };
            db.collection("Ban_Data").find().sort(mysort).toArray(async (err, result) => {
              msg.channel.send({embed: {
                color: 3447003,
                author: { name: bot.user.username, icon_url: bot.user.displayAvatarURL },
                description: `\`\`\`diff\n!=== [ Ban List ] ===!\n\n${result.map(c=>`+ ${c._id} \n- Reason: ${c.reason} \n- Issued: ${c.time}`).join("\n\n")}\`\`\``,
                timestamp: new Date(),
                footer: {
                icon_url: bot.user.avatarURL,
                text: '© Merc'
              }
            }});
            });
           
        } else {
            db.collection("Ban_Data").findOne({
                _id: parseInt(message.content)
            }, function(err, result) {
                if (result === null) {
                    bot.sendError(msg,"Could not find user!");
                    return;
                } else {
                    msg.channel.send({embed: {
                        color: 3447003,
                        author: { name: bot.user.username, icon_url: bot.user.displayAvatarURL },
                        description: `\`\`\`diff\n!=== [Specific Ban List ] ===!\n\n+ ${result._id} \n- Reason: ${result.reason}\n- Issued: ${result.time}\`\`\``,
                        timestamp: new Date(),
                        footer: {
                        icon_url: bot.user.avatarURL,
                        text: '© Merc'
                      }
                    }});
                }
            });
        }
    });
    collector.on('end', collection => {
        if (collection.size === 0) {
            bot.sendError(msg, "Failure: Collector timed out!");
            return;
        }
    });
    
})
};

module.exports.conf = {
  enabled: true,
  guildOnly: false,
  permLevel: 0
};

module.exports.help = {
  name : "listbans",
  description: "Returns all commands and details on the commands",
  usage: "listbans"
};
