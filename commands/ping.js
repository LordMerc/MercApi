module.exports.run = (bot, msg, params = []) => {
  msg.channel.send("Pinging..").then(m => {
    m.edit({embed: {
      color: 3447003,
      description: `Pong! Latency is ${m.createdTimestamp - msg.createdTimestamp}ms. API Latency is ${Math.round(bot.ping)}ms`
    }});
  })

};

module.exports.conf = {
  enabled: true, // not used yet
  guildOnly: false, // not used yet
  permLevel: 0 // Permissions Required, higher is more power
};

module.exports.help = {
  name : "ping",
  description: "Return time ping",
  usage: "ping"
};
