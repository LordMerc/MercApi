module.exports.run = (bot, msg, params = []) => {
  const code = params.join(" ");
  bot.user.setPresence({ game: { name: code, type: 0 } });;
};

module.exports.conf = {
  enabled: true, // not used yet
  guildOnly: false, // not used yet
  permLevel: 4 // Permissions Required, higher is more power
};

module.exports.help = {
  name : "status",
  description: "Sets game status",
  usage: "status <argument>"
};
