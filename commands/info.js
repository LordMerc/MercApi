const Discord = require('discord.js');
module.exports.run = (bot, msg, params = []) => {
  msg.channel.send({embed: {
    color: 3447003,
    title: "Bot Details",
    description: "Details on bot",
    author: {
    name: bot.user.username,
    icon_url: bot.user.displayAvatarURL
  },
    fields: [
{
  name: 'Mem Usage',
  value: (process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2) + ' MB',
  inline: true
},
{
  name: 'Swap Size',
  value: (process.memoryUsage().rss / 1024 / 1024).toFixed(2) + ' MB',
  inline: true
},
{
  name: 'Uptime',
  value: bot.utils.humanizeDuration(bot.uptime),
  inline: true
},
{
  name: 'Users',
  value: msg.guild.members.size,
  inline: true
},
{
  name: 'Servers',
  value: bot.guilds.size,
  inline: true
},
{
  name: 'Discord Version',
  value: Discord.version,
  inline: true
},

],
  }});
};

module.exports.conf = {
  enabled: true,
  guildOnly: true,
  permLevel: 0
};

module.exports.help = {
  name: "info",
  description: "Provides some information about this bot.",
  usage: "info"
};
