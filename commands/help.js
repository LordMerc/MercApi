const Discord = require("discord.js")
module.exports.run = (bot, msg, params) => {
    if (!params[0]) {
      try {
        msg.author.send({embed: {
          color: 3447003,
          author: { name: bot.user.username, icon_url: bot.user.displayAvatarURL },
          description: `\`\`\`diff\n!=== [ Command List ] ===!\n\n${bot.commands.map(c=>`+ ${c.help.name} \n- Description: ${c.help.description} \n- Usage: ${c.help.usage}`).join("\n\n")}\`\`\``,
          timestamp: new Date(),
          footer: {
          icon_url: bot.user.avatarURL,
          text: '© Merc'
         }
        }});
      } catch (err) {
        msg.reply("Could not DM you commands!")
      }
  } else {
    let command = params[0];
    if(bot.commands.has(command)) {
      command = bot.commands.get(command);
      try {
        let embed = new Discord.RichEmbed()
        .setColor("RANDOM")
        .setDescription("Command Help")
        .addField("Command Name",command.help.name)
        .addField("Command Description",command.help.description)
        .addField("Command Usage",command.help.usage)
        msg.author.send({embed})
      } catch (err) {
        msg.reply("Could not DM you commands!")
      }
    }
  }
};

module.exports.conf = {
  enabled: true,
  guildOnly: false,
  permLevel: 0
};

module.exports.help = {
  name : "help",
  description: "Returns all commands and details on the commands",
  usage: "help [command]"
};
