var MongoClient = require('mongodb').MongoClient;
var Discord = require("discord.js");
var crypto = require('crypto');
var lodash = require('lodash');
var waitUntil = require('wait-until');
var url = "";

module.exports.run = (bot, msg, params = []) => {
    const grabgroup = async(msg) => {
        let client = await MongoClient.connect(url)
        const db = client.db("MercApi")
        let result = await db.collection("Guild_Data").findOne({_id: msg.guild.id})
        return result
        client.close()
    }
    const grabsubs = async(msg) => {
        let client = await MongoClient.connect(url)
        const db = client.db("MercApi")
        let result = await db.collection("Guild_Data").findOne({_id: msg.guild.id})
        return result
        client.close()
    }
    function randomValueHex (len) {
        return crypto.randomBytes(Math.ceil(len/2))
            .toString('hex') // convert to hexadecimal format
            .slice(0,len);   // return required number of characters
    }
    async function resolveGroupBinding(binding, userid) {
        // Check if the return value of this method has already been
        // cached in memory. If so, return that.
        let returnValue = false
    
        try {
            let rank = await bot.rbx.getRankInGroup({group: binding.groupId, userId: userid});
            if (rank > 0) {
                returnValue = true
            } else {
               
            }
        } catch (e) {
            console.log(e)
        }
    
        // Cache the return value in memory.
       
        return returnValue
      }
      async function getStuff(groupid){
        let returnValue
        try {
            let roles = await bot.rbx.getRoles({group:groupid})
            returnValue = roles
        } catch (err) {
            console.log(err)
        }
        return returnValue
      }
      async function resolveGroupRankBinding(binding,id,userid) {
        let returnValue = false
    
        try {
            let rank = await bot.rbx.getRankNameInGroup({group: binding, userId: userid});
            if (rank === id) {
                console.log(userid + ' CAN GET ROLE OF ' + id)
                returnValue = true
            } else {
                console.log(userid + ' CANNOT GET ROLE OF ' + id)
            }
        } catch (e) {
            console.log(e)
        }
    
        // Cache the return value in memory.
       
        return returnValue
      }
    MongoClient.connect(url, function(err, client) {
        const db = client.db("MercApi")
        if (err) {
            console.log("Failed (" + err + ")");
            return false;
        }
       
        db.collection("Verification_Data").findOne({_id: msg.author.id}, async function(err, result) {
            let roles_given = []
            let roles_removed = []
            let idx = 0;
            if (!result) {
                let embed = new Discord.RichEmbed()
                embed.setTitle("MercApi - Role Giving Prompt")
                embed.setColor(0xCC0000)
                embed.setDescription("ERROR DETECTED")
                embed.setTimestamp()
                embed.addField("Error Code","User not verified")
                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                await msg.channel.send({embed})
                return
            }
            try {
                let embed = new Discord.RichEmbed()
                embed.setTitle("MercApi")
                embed.setColor(0x00AE86)
                let group = await grabgroup(msg)
                if (group.groupId === 0) {
                    let embed = new Discord.RichEmbed()
                    embed.setTitle("MercApi - Role Giving Prompt")
                    embed.setColor(0xCC0000)
                    embed.setDescription("ERROR DETECTED")
                    embed.setTimestamp()
                    embed.addField("Error Code","No GroupId has been given to guild")
                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                    await msg.channel.send({embed})
                    return
                }
                embed.setDescription("Role Giving Prompt.")
                embed.setTimestamp()
                embed.addField("Group Detected", group.groupId)
                let subs = await grabsubs(msg)
                if (subs.subgroups === null || subs.subgroups === undefined) {
                } else {
                    for (let binding of subs.subgroups) {
                        let access = await resolveGroupBinding(binding,result.userid)
                        if (access === true) {
                            if (!msg.member.roles.find("name",binding.role) && msg.guild.roles.find('name',binding.role)) {
                                await roles_given.push(binding.role)
                                await msg.member.addRole(msg.guild.roles.find('name',binding.role))
                            } else if (access === false) {
                                if (msg.member.roles.find("name",binding.role) && msg.guild.roles.find('name',binding.role)) {
                                    await roles_removed.push(binding.role)
                                    await msg.member.removeRole(msg.guild.roles.find('name',binding.role))
                                }
                            }
                        }
                    }
                }
                let main_group = await getStuff(group.groupId)
                for (let role of main_group) {
                    console.log(group.groupId,role.Name,result.userid)
                    let access = await resolveGroupRankBinding(group.groupId,role.Name, result.userid)
                    console.log(access)
                    if (access === true) {
                        if (!msg.member.roles.find("name",role.Name) && msg.guild.roles.find('name',role.Name)) {
                           await roles_given.push(role.Name)
                           await msg.member.addRole(msg.guild.roles.find('name',role.Name))
                        }
                    } else {
                            if (msg.member.roles.find("name",role.Name) && msg.guild.roles.find('name',role.Name)) {
                               await roles_removed.push(role.Name)
                               await msg.member.removeRole(msg.guild.roles.find('name',role.Name))
                           }
                    }
                }
                if (roles_given.length > 0) {
                    embed.addField("Roles given", roles_given.map(role => "**" + role + "**").join("\n"),true)
                }else{
                    embed.addField("Roles given", "None",true)
                }
                if (roles_removed.length > 0) {
                    embed.addField("Roles removed", roles_removed.map(role => "**" + role + "**").join("\n"),true)
                } else {
                    embed.addField("Roles Removed", "None",true)
                }
                embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                await msg.channel.send({embed})
            } catch(err){

            }
        })
        client.close()
    });
};

module.exports.conf = {
    enabled: true, // not used yet
    guildOnly: true, // not used yet
    permLevel: 0 // Permissions Required, higher is more power
};

module.exports.help = {
    name: "getrole",
    description: "getrole from roblox group ok",
    usage: "getrole"
};