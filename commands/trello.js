const Trello = require("trello")
const Promise = require("bluebird")
const Discord = require("discord.js")
const trello = new Trello("379ac65729805f0e41bfc26fc4afd889", "86a51d76d087d72c1925ecc324a1909b58990d83a411dd0603a1137b9ce98cba");
let Classes = [
    {name: "Phase 1",value:'5a6e8c7c4c173c8712234d9a',role_id:2},
    {name: "Phase 2",value:'5a6e8c90d04ecf309c6b803d',role_id:3},
    {name: "Phase 3",value:'5a6e8caa01169dd08f514442',role_id:4},
    {name: "Graduated",value:'5a6e846daabaf58cc88788fc',role_id:6}
]
let Academy_Group = 3643718
module.exports.run = (bot, msg, params = []) => {
    if (params[0] === null || params[0] === undefined) {
        let embed = new Discord.RichEmbed()
        embed.setColor(0xCC0000)
        embed.setDescription("**No request made**")
        embed.setAuthor("MercApi v2","http://www.freeiconspng.com/uploads/x-png-23.png") 
        return msg.reply({embed})
    }
    if (params[0].toLowerCase() === "retrieve") {
        let User = params[1]
        if (User === null || User === undefined) {
            return msg.reply("No user supplied")
        }
        let Found_List = null
        let Found_Card = null
        trello.getListsOnBoard("76mkNlHU").then((lists) => {
                new Promise(async function(r1,rj){
                    for (let List of lists) {
                    await trello.getCardsOnList(List.id).then(function(Cards){
                            for (let Card of Cards) {
                                if (User.toLowerCase() === Card.name.toLowerCase()) {
                                    Found_Card = Card
                                    Found_List = List
                                }
                            }
                        })
                    }
                r1(Found_Card,Found_List)
                }).then(async function(card,list){
                    if (card != undefined) {
                        let Player = await bot.rbx.getIdFromUsername(Found_Card.name.toLowerCase())
                        let Rank = await bot.rbx.getRankNameInGroup(Academy_Group,Player)
                        let embed = new Discord.RichEmbed()
                        embed.setAuthor("MercApi v2","https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png") 
                        embed.setColor(0x00AE86)
                        embed.setTitle(`Trello Information for User: ${Found_Card.name}`)
                        embed.setURL(Found_Card.url)
                        embed.setDescription(`Username: **${Found_Card.name}**\nCard's Parent List: **${Found_List.name}**\nGroup Rank: **${Rank}**`)
                        return msg.reply({embed})
                    } else {
                        let embed = new Discord.RichEmbed()
                        embed.setColor(0xCC0000)
                        embed.setDescription("**No card found with that username**")
                    // embed.setTimestamp()
                        embed.setAuthor("MercApi v2","http://www.freeiconspng.com/uploads/x-png-23.png") 
                        return msg.reply({embed})
                    }
                })
        })
    } else if (params[0].toLowerCase() === "inspect") {
        let Full_Request = params.slice(1).join(" ")
        new Promise(async function(r1,rj) {
            let found = false
           await Classes.find(function(element) {
                if (element.name.toLowerCase() === Full_Request.toLowerCase()) {
                  found = element
                }
            })
            r1(found)
        }).then(async function(info) {
            let Correct_People = []
            let Incorrect_People = []
            let Correct_List = []
            let Incorrect_List = []
            if (info != false) {
                let Cards = await trello.getCardsOnList(info.value)
                let GroupPlayers = await bot.rbx.getPlayers({group:Academy_Group,rank:info.role_id})
                new Promise(async function(r,rj){
                   for (let Player of GroupPlayers.players) {
                    let result = Cards.find(Card => Card.name.toLowerCase() === Player.name.toLowerCase())
                    if (result) {
                        Correct_People.push(Player.name)
                    } else {
                        Incorrect_People.push(Player.name)
                    }
                   }
                   for (let Card of Cards) {
                       let result = GroupPlayers.players.find(Player => Player.name.toLowerCase() === Card.name.toLowerCase())
                       if (result) {
                           Correct_List.push(Card.name)
                       } else {
                            Incorrect_List.push(Card.name)
                       }
                   }
                r()
                }).then(function() {
                    let embed = new Discord.RichEmbed()
                    embed.setAuthor("MercApi v2","https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png") 
                    embed.setColor(0x00AE86)
                    embed.setTitle(`Trello Information for List: ${info.name}`)
                    embed.setDescription(`Number of Cards in Trello List: **${Cards.length}**\nNumber of Group Ranked: **${GroupPlayers.players.length}**\nUnfound People: **${Incorrect_People.length}**\nIncorrectly Placed Cards: **${Incorrect_List.length}**`)
                    if (Incorrect_List.length > 0) {
                        embed.addField("Incorrectly Placed Cards",Incorrect_List.map(card => "**" + card + "**").join("\n"))
                    }
                    if (Incorrect_People.length > 0) {
                        embed.addField("Unfound People",Incorrect_People.map(person => "**" +person + "**").join("\n"))
                    }
                    //msg.reply(`Correct Request\nList length: **${Cards.length}**\nGroup Rank length: **${GroupPlayers.players.length}**\nCorrect Length: **${Correct_People.length}**\nIncorrect_List Length: **${Incorrect_List.length}**`)
                  //  msg.reply(`\nIncorrect_List List:[${Incorrect_List.map(p => '**' + p + '**').join(", ")}]\nMissing_People List:[${Incorrect_People.map(p => '**' + p + '**').join(", ")}]`)
                    return msg.reply({embed})
                })
            } else {
                let embed = new Discord.RichEmbed()
                embed.setColor(0xCC0000)
                embed.setDescription("**Unidentified arguments given**")
               // embed.setTimestamp()
                embed.setAuthor("MercApi v2","http://www.freeiconspng.com/uploads/x-png-23.png") 
                return msg.reply({embed})
            }
        })
    }
  };
  
  module.exports.conf = {
    enabled: true, // not used yet
    guildOnly: false, // not used yet
    permLevel: 0 // Permissions Required, higher is more power
  };
  
  module.exports.help = {
    name : "trello",
    description: "Bot says what you say",
    usage: "trello <argument>"
  };
  