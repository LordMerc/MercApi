  async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
  }
const MongoClient = require('mongodb').MongoClient
const Discord = require("discord.js")
const url = ""
async function GrabUser(id) {
    let client = await MongoClient.connect(url)
    const db = client.db("MercApi")
    let result = await db.collection("Verification_Data").findOne({_id: id})
    if (result) {
        return result
    } else {
        return null
    }
}
function EmbedMaker(Data) {
    let Title = Data.Title || null
    let Color = Data.Color || "RANDOM"
    let Description = Data.Description || null
    let Footer = Data.Footer || null
    let Fields = Data.Fields || null
    let Timestamp = Data.Timestamp || null
    let Thumbnail = Data.Thumbnail || null
    let embed = new Discord.RichEmbed()
        .setColor(Color)
        .setDescription(Description)
        if (Footer){ embed.setFooter(Footer) }
        if (Title){ embed.setTitle(Title) }
        if (Thumbnail){ embed.setThumbnail(Title) }
        if (Timestamp){ embed.setTimestamp() }
        if (Fields){Fields.forEach(Data => { embed.addField(Data.Title,Data.Value) });}
    return {embed}
}
module.exports.run = async (bot, msg, params = []) => {
    let Prompt_Stopped = false
    let Author = msg.author
    let Author_Roblox = await GrabUser(Author.id)
    if (!Author_Roblox) {
        await msg.channel.send(EmbedMaker({Title:'Invalid Permission Level',Description:"You do not have the ability to access the panel!",Color:"RED"}))
        return
    }
    let Group_Role = await bot.rbx.getRankInGroup(1001180,Author_Roblox.userid)
    if (Group_Role < 50) {
        await msg.channel.send(EmbedMaker({Title:'Invalid Permission Level',Description:"You do not have the ability to access the panel!",Color:"RED"}))
        return
    }
    let options = ["setrank","demote","promote","shout"]
    
    await msg.channel.send(EmbedMaker({Title:'Permission Granted',Description:`Welcome, ${await bot.rbx.getUsernameFromId(Author_Roblox.userid)}. Please select an option to continue\n${options.map(opt => `**${opt}**`).join("\n")}`,Color:"BLUE",Timestamp:true,Footer:"Say 'cancel' to cancel this"}))
    const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1 });
    collector.on('collect', async (message) => {
        if (message.content.toLowerCase() == "cancel") {
            Prompt_Stopped = true
            collector.stop('Cancelled')
            await msg.channel.send(EmbedMaker({Description:`Successfully cancelled prompt`,Color:"GREEN"}))
            return
        }
        if (message.content.toLowerCase() == "setrank") {
            let People = []
            const People_Collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id);
            await msg.channel.send(EmbedMaker({Title:'Group Admin Panel | Set Rank',Description:`Please list the **UserId** or **Username** of the people, or person if one, you wish to set the rank of seperated by a **comma**. Reply **end.prompt** to stop this step.`,Color:"RED",Footer:"Say 'cancel' to cancel this"}))
            People_Collector.on('collect', async (response) => {
                if (response.content.toLowerCase() === "cancel") {
                    People_Collector.stop('Cancelled')
                    await msg.channel.send(EmbedMaker({Description:`Successfully cancelled prompt`,Color:"GREEN"}))
                    return
                }
                if (response.content !== "end.prompt") {
                    let Test_response = response.content.replace(/\s+/, "") 
                    let This_Params = Test_response.split(",").slice(0)
                    let Duplicates = []
                    await asyncForEach(This_Params, async (obj) => {
                        let name
                        let id
                        try {
                            name = await bot.rbx.getUsernameFromId(obj)
                            id = await bot.rbx.getIdFromUsername(name)
                        } catch (err) {
                            try {
                                id = await bot.rbx.getIdFromUsername(obj)
                                name = await bot.rbx.getUsernameFromId(id)
                            } catch (err) {
                                name = null
                                id = null
                            }
                        }
                        if (name && id) {
                            if (People.includes(id)) {
                                Duplicates.push(id)
                            } else {
                                People.push(id)
                            }
                        }
                      })
                    if (Duplicates.length === 1) {
                        let name = await bot.rbx.getUsernameFromId(Duplicates[0])
                        msg.channel.send(EmbedMaker({Title:'Duplicate Entry detected',Description:`The following user has already been specified\n[**USERNAME**]: ${name} [**USERID**]: ${Duplicates[0]}`,Color:"RED"}))
                    } else if (Duplicates.length > 1) {
                        let WithNames = []
                        await asyncForEach(Duplicates, async (obj) => {
                            let name = await bot.rbx.getUsernameFromId(obj)
                            WithNames.push(`[**USERNAME**]: ${name} [**USERID**]: ${obj}`)
                        })
                        msg.channel.send(EmbedMaker({Title:'Duplicate Entries detected',Description:`The following users have already been specified\n${WithNames.map(o => o).join("\n")}`,Color:"RED"}))
                    }
                } else {
                    People_Collector.stop()
                }
            })
            People_Collector.on('end', async () => {
                console.log(People)
                let WithNames = []
                  await asyncForEach(People, async (obj) => {
                    let name = await bot.rbx.getUsernameFromId(obj)
                    WithNames.push(`[**USERNAME**]: ${name} [**USERID**]: ${obj}`)
                  })
                if (People.length === 0) {
                    return
                } else {
                    msg.channel.send(EmbedMaker(
                        {
                            Title:'Array content',
                            Description: WithNames.map(o => o).join("\n"),
                            Color:"RED"
                        }))
                    return
                }
            })

        } else if (message.content.toLowerCase() == "demote") {

        } else if (message.content.toLowerCase() == "promote") {

        } else if (message.content.toLowerCase() == "shout") {

        } else {
            await msg.channel.send(EmbedMaker({Title:'Invalid Request',Description:`Requested **${message.content}** is not a valid option`,Color:"RED"}))
            return
        }
    })
    collector.on('end', async (collection) => {
        if (collection.size === 0 && Prompt_Stopped === false) {
            await msg.channel.send(EmbedMaker({Title:'Empty response',Description:`No response was given in time`,Color:"RED"}))
            return
        }
    })
    return
  };
  
  module.exports.conf = {
    enabled: true, // not used yet
    guildOnly: false, // not used yet
    permLevel: 0 // Permissions Required, higher is more power
  };
  
  module.exports.help = {
    name : "groupadmin",
    description: "Access group admin panel",
    usage: "groupadmin"
  };
  