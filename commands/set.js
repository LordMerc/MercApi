var MongoClient = require('mongodb').MongoClient;
var Discord = require("discord.js");
var lodash = require('lodash');
var url = "";
module.exports.run = (bot, msg, params = []) => {
        MongoClient.connect(url, async function(err, client) {
            const db = client.db("MercApi")
            if (err) {
                console.log("Failed (" + err + ")");
                return false;
            }
            if (params[0] === null || params[0] === undefined) {
                return msg.reply(`Please specify your option: **Prefix**, **Logs** (must be used in channel desired), **Subgroups**, **GroupId**`)
            }
            if (params[0].toLowerCase() === "guild") {
                db.collection("Guild_Data").findOne({
                    _id: msg.guild.id
                }, async function(err, result) {
                    if (result === null) {
                        var myobj = {
                            _id: msg.guild.id,
                            groupId: 0,
                            groupName: "",
                            logs: msg.channel.name,
                            prefix: "-"
                        };
                        db.collection("Guild_Data").insertOne(myobj, async function(err, res) {
                            if (err) throw err;
                            let embed = new Discord.RichEmbed()
                            embed.setTitle("MercApi - Guild setup")
                            embed.setColor(0x00AE86)
                            embed.setDescription(`Guild setup complete! You may now customize the settings available to you!\nThese settings are open to be edited:\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)
                            embed.setTimestamp()
                            embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                            await msg.reply({embed})
                            client.close()
                            return
                        })
                    } else {
                        let embed = new Discord.RichEmbed()
                        embed.setTitle("MercApi - Guild setup")
                        embed.setColor(0xCC0000)
                        embed.setDescription("ERROR DETECTED")
                        embed.setTimestamp()
                        embed.addField("Error Code","Guild does not require setup")
                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                        await msg.reply({embed})
                       
                        return
                    }
                });
            };
            if (params[0].toLowerCase() === "prefix") {
                db.collection("Guild_Data").findOne({
                    _id: msg.guild.id
                }, async function(err, result) {
                    if (result === null) {
                        let embed = new Discord.RichEmbed()
                        embed.setTitle("MercApi - Guild setup (Prefix)")
                        embed.setColor(0xCC0000)
                        embed.setDescription("ERROR DETECTED")
                        embed.setTimestamp()
                        embed.addField("Error Code","Guild is not setup")
                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                        await msg.reply({embed})
                        
                        return
                    } else {
                        msg.reply("Please state a prefix : **Must be 1 character long**");
                        const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1 });
                        collector.on('collect', async (message) => {
                            if (message.content.length === 1) {
                                var Old_Prefix = {
                                    _id: msg.guild.id,
                                    prefix: result.prefix
                                };
                                var New_Prefix = {
                                    $set: {
                                        prefix: message.content
                                    }
                                };
                                db.collection("Guild_Data").updateOne(Old_Prefix, New_Prefix, async function(err3, res) {
                                    if (err3) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Prefix - Edit)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code",err3)
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                        client.close()
                                        return
                                    } else {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Prefix - Edit)")
                                        embed.setColor(0x00AE86)
                                        embed.setDescription(`Successfully changed prefix!`)
                                        embed.addField("Old Prefix",result.prefix)
                                        embed.addField("New Prefix",message.content)
                                        embed.setTimestamp()
                                        embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                        await msg.reply({embed})
                                        client.close()
                                        return
                                    }
                                });
                            } else {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (Prefix - Edit)")
                                embed.setColor(0xCC0000)
                                embed.setDescription("ERROR DETECTED")
                                embed.setTimestamp()
                                embed.addField("Error Code","Too many characters specified")
                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                await msg.reply({embed})
                                
                                return
                            }

                        });
                        collector.on('end', async (collection) => {
                            if (collection.size === 0) {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (Prefix - Edit)")
                                embed.setColor(0xCC0000)
                                embed.setDescription("ERROR DETECTED")
                                embed.setTimestamp()
                                embed.addField("Error Code","Collector timed out")
                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                await msg.reply({embed})
                                return
                            }
                        });
                    }
                });
            };
            if (params[0].toLowerCase() === "logs") {
                db.collection("Guild_Data").findOne({
                    _id: msg.guild.id
                }, async function(err, result) {
                    if (result === null) {
                        let embed = new Discord.RichEmbed()
                        embed.setTitle("MercApi - Guild setup (Logs)")
                        embed.setColor(0xCC0000)
                        embed.setDescription("ERROR DETECTED")
                        embed.setTimestamp()
                        embed.addField("Error Code","Guild is not setup")
                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                        await msg.reply({embed})
                       
                        return
                } else {
                        var Old_Logs = {
                            _id: msg.guild.id,
                            logs: result.logs
                        };
                        var New_Logs = {
                            $set: {
                                logs: msg.channel.name
                            }
                        };
                        db.collection("Guild_Data").updateOne(Old_Logs, New_Logs, async function(err3, res) {
                            if (err3) {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (Logs - Edit)")
                                embed.setColor(0xCC0000)
                                embed.setDescription("ERROR DETECTED")
                                embed.setTimestamp()
                                embed.addField("Error Code",err3)
                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                await msg.reply({embed})
                               
                                return
                            } else {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (Logs - Edit)")
                                embed.setColor(0x00AE86)
                                embed.setDescription(`Successfully changed logs channel!`)
                                embed.addField("Old Logs Channel",result.logs)
                                embed.addField("New Logs Channel",msg.channel.name)
                                embed.setTimestamp()
                                embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                await msg.reply({embed})
                               
                                return
                            }
                           
                        });
                    }
                });
            };
            if (params[0].toLowerCase() === "sub" || params[0].toLowerCase() === "subgroups") {
                db.collection("Guild_Data").findOne({
                    _id: msg.guild.id
                }, async function(err, result) {
                    if (result === null) {
                        let embed = new Discord.RichEmbed()
                        embed.setTitle("MercApi - Guild setup (Subgroups)")
                        embed.setColor(0xCC0000)
                        embed.setDescription("ERROR DETECTED")
                        embed.setTimestamp()
                        embed.addField("Error Code","Guild is not setup")
                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                        await msg.reply({embed})
                        
                        return
                    } else {
                        if (!result.subgroups) {
                            var Old_Data = {
                                _id: msg.guild.id,
                            };
                            var New_Data = {
                                $set: {
                                    subgroups:[]
                                }
                            };
                            db.collection("Guild_Data").updateOne(Old_Data, New_Data, async function(err3, res) {
                                if (err3) {
                                    let embed = new Discord.RichEmbed()
                                    embed.setTitle("MercApi - Guild setup (Subgroups)")
                                    embed.setColor(0xCC0000)
                                    embed.setDescription("ERROR DETECTED")
                                    embed.setTimestamp()
                                    embed.addField("Error Code",err3)
                                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                    await msg.reply({embed})
                                   
                                    return
                                } else {
                                    let embed = new Discord.RichEmbed()
                                    embed.setTitle("MercApi - Guild setup (Subgroups)")
                                    embed.setColor(0x00AE86)
                                    embed.setDescription(`Successfully added subgroups array to guild data`)
                                    embed.setTimestamp()
                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                    await msg.reply({embed})
                                  
                                    return
                                }
                               
                            });
                        } else {
                        msg.reply("Do you wish to **Edit**, **Add**, or **Remove** a subgroup? You can view current subgroups with **View**, or say **Cancel** to end this prompt");
                        const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1 });
                        collector.on('collect', async (message) => {
                            if (message.content.toLowerCase() == "cancel") {
                                msg.reply("Ended prompt");
                                return;
                            } else if (message.content.toLowerCase() == "view") {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (Subgroups - View)")
                                embed.setColor(0x00AE86)
                                embed.setDescription(`Current subgroups for guild`)
                                embed.setTimestamp()
                                for (var a = 0; a < result.subgroups.length; a++) {
                                   embed.addField("GroupId: " + result.subgroups[a].groupId,"Role: **" + result.subgroups[a].role + "**",true)
                                }
                                embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                await msg.reply({embed})
                                
                                return
                            } else if (message.content.toLowerCase() == "edit") {
                                msg.reply("Please enter a valid **GroupId**");
                                const collector2 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1 });
                                collector2.on('collect', async (message2) => {
                                    let id = parseInt(message2.content);
                                    if (isNaN(id)) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Edit)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","Invalid number")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                        return
                                    }
                                    var Finder = await lodash.filter(result.subgroups, x => x.groupId === id);
                                    if (Finder.length === 0) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Edit)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","No subgroup found with requested GroupId")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                        return
                                    } else {
                                        msg.reply("Please reply with a valid **Discord Role**. NOTE: **Case_Sensitive**");
                                        const collector3 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1 });
                                        collector3.on('collect', async (message3) => {
                                            let role = await msg.guild.roles.find('name',message3.content);
                                            if (!role) {
                                                let embed = new Discord.RichEmbed()
                                                embed.setTitle("MercApi - Guild setup (Subgroups - Edit)")
                                                embed.setColor(0xCC0000)
                                                embed.setDescription("ERROR DETECTED")
                                                embed.setTimestamp()
                                                embed.addField("Error Code","No role found with requested name")
                                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                                await msg.reply({embed})
                                                return
                                            } else {
                                                for(var a = 0; a < result.subgroups.length; a++) {
                                                    if(result.subgroups[a].groupId == id) {
                                                        delete result.subgroups[a];
                                                    }
                                                }
                                                result.subgroups = result.subgroups.filter(function(x) {
                                                    
                                                    return (x !== (undefined || 'a' || ''));
                                                    
                                                    });
                                                var Test = {};
                                                Test.groupId = id;
                                                Test.role = role.name;
                                                result.subgroups.push(Test);
                                                var Old_Prefix = {
                                                    _id: msg.guild.id,
                                                };
                                                var New_Prefix = {
                                                    $set: {
                                                        subgroups: result.subgroups
                                                    }
                                                };
                                                db.collection("Guild_Data").updateOne(Old_Prefix, New_Prefix, async function(err3, res) {
                                                    if (err3) {
                                                        let embed = new Discord.RichEmbed()
                                                        embed.setTitle("MercApi - Guild setup (Subgroups - Edit)")
                                                        embed.setColor(0xCC0000)
                                                        embed.setDescription("ERROR DETECTED")
                                                        embed.setTimestamp()
                                                        embed.addField("Error Code",err3)
                                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                                        await msg.reply({embed})
                                                        return
                                                    } else {
                                                        let embed = new Discord.RichEmbed()
                                                        embed.setTitle("MercApi - Guild setup (Subgroups - Edit)")
                                                        embed.setColor(0x00AE86)
                                                        embed.setDescription(`Successfully changed **${id}**'s role to **${role.name}**!`)
                                                        embed.setTimestamp()
                                                        embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                        await msg.reply({embed})
                                                        
                                                        return
                                                    }
                                                   
                                                });

                                            }
                                        });
                                        collector3.on('end', async (collection) => {
                                            if (collection.size === 0) {
                                                let embed = new Discord.RichEmbed()
                                                embed.setTitle("MercApi - Guild setup (Subgroups - Edit)")
                                                embed.setColor(0xCC0000)
                                                embed.setDescription("ERROR DETECTED")
                                                embed.setTimestamp()
                                                embed.addField("Error Code","Collector timed out")
                                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                                await msg.reply({embed})
                                                return
                                            }
                                        });
                                    }
                                });
                                collector2.on('end', async (collection) => {
                                    if (collection.size === 0) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","Collector timed out")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                        return
                                    }
                                });
                            } else if (message.content == "remove" || message.content == "Remove" ) {
                                msg.reply("Please enter a valid **GroupId**");
                                const collector2 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1 });
                                collector2.on('collect', async (message2) => {
                                    let id = parseInt(message2.content);
                                    if (isNaN(id)) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Remove)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","Invalid number")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                        return
                                    }
                                    var Finder = lodash.filter(result.subgroups, x => x.groupId === id);
                                    console.log(Finder);
                                    if (Finder.length === 0) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Edit)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","No subgroup found with requested GroupId")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                        return
                                    } else {
                                        for(var a = 0; a < result.subgroups.length; a++) {
                                            if(result.subgroups[a].groupId == id) {
                                                delete result.subgroups[a];
                                            }
                                        }
                                        result.subgroups = result.subgroups.filter(function(x) {
                                            
                                            return (x !== (undefined || 'a' || ''));
                                            
                                            });
                                            
                                        var Old_Prefix = {
                                            _id: msg.guild.id,
                                        };
                                        var New_Prefix = {
                                            $set: {
                                                subgroups: result.subgroups
                                            }
                                        };
                                        db.collection("Guild_Data").updateOne(Old_Prefix, New_Prefix,async function(err3, res) {
                                            if (err3) {
                                                let embed = new Discord.RichEmbed()
                                                embed.setTitle("MercApi - Guild setup (Subgroups - Remove)")
                                                embed.setColor(0xCC0000)
                                                embed.setDescription("ERROR DETECTED")
                                                embed.setTimestamp()
                                                embed.addField("Error Code",err3)
                                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                                await msg.reply({embed})
                                                return
                                            } else {
                                                let embed = new Discord.RichEmbed()
                                                embed.setTitle("MercApi - Guild setup (Subgroups - Remove)")
                                                embed.setColor(0x00AE86)
                                                embed.setDescription(`Successfully removed **${id}** from subgroups!`)
                                                embed.setTimestamp()
                                                embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                await msg.reply({embed})
                                               
                                                return
                                            }
                                           
                                        });

                                    }
                                });
                                collector2.on('end', async (collection) => {
                                    if (collection.size === 0) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Remove)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","Collector timed out")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                        return
                                    }
                                });
                            } else if (message.content == "clr.all") {
                                var Old_Prefix = {
                                    _id: msg.guild.id,
                                };
                                var New_Prefix = {
                                    $set: {
                                        subgroups: []
                                    }
                                };
                                db.collection("Guild_Data").updateOne(Old_Prefix, New_Prefix, async function(err3, res) {
                                    if (err3) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Remove)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code",err3)
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                       
                                        return
                                    } else {
                                        return;
                                    }
                                  
                                });
                            } else if (message.content == "add" || message.content == "Add" ) {
                                msg.reply("Please reply with a valid **GroupId**");
                                const collector2 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1 });
                                collector2.on('collect', async (message2) => {
                                    let id = parseInt(message2.content);
                                    if (isNaN(id)) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Add)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","Invalid number specified")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                        return
                                    }
                                    var Finder = lodash.filter(result.subgroups, x => x.groupId === id);
                                    console.log(Finder);
                                    if (Finder.length === 0) {
                                        msg.reply("Please reply with a valid **Discord Role**. NOTE: **Case_Sensitive**");
                                        const collector3 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1 });
                                        collector3.on('collect', async (message3) => {
                                            let role = msg.guild.roles.find('name',message3.content);
                                            if (!role) {
                                                let embed = new Discord.RichEmbed()
                                                embed.setTitle("MercApi - Guild setup (Subgroups - Add)")
                                                embed.setColor(0xCC0000)
                                                embed.setDescription("ERROR DETECTED")
                                                embed.setTimestamp()
                                                embed.addField("Error Code","Unable to locate role specified")
                                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                                await msg.reply({embed})
                                                return
                                            } else {
                                                let subs = result.subgroups;
                                                var Test = {};
                                                Test.groupId = id;
                                                Test.role = role.name;
                                                subs.push(Test);
                                                var Old_Prefix = {
                                                    _id: msg.guild.id,
                                                };
                                                var New_Prefix = {
                                                    $set: {
                                                        subgroups: subs
                                                    }
                                                };
                                                db.collection("Guild_Data").updateOne(Old_Prefix, New_Prefix, async function(err3, res) {
                                                    if (err3) {
                                                        let embed = new Discord.RichEmbed()
                                                        embed.setTitle("MercApi - Guild setup (Subgroups - Add)")
                                                        embed.setColor(0xCC0000)
                                                        embed.setDescription("ERROR DETECTED")
                                                        embed.setTimestamp()
                                                        embed.addField("Error Code",err3)
                                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                                        await msg.reply({embed})
                                                        
                                                        return
                                                    } else {
                                                        let embed = new Discord.RichEmbed()
                                                        embed.setTitle("MercApi - Guild setup (Subgroups - Add)")
                                                        embed.setColor(0x00AE86)
                                                        embed.setDescription(`Successfully added **${id}** to subgroups with role being **${role.name}**!`)
                                                        embed.setTimestamp()
                                                        embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                        await msg.reply({embed})
                                                        
                                                        return
                                                    }
                                                  
                                                });
                                            }
                                        });
                                        collector3.on('end', async (collection) => {
                                            if (collection.size === 0) {
                                                let embed = new Discord.RichEmbed()
                                                embed.setTitle("MercApi - Guild setup (Subgroups - Add)")
                                                embed.setColor(0xCC0000)
                                                embed.setDescription("ERROR DETECTED")
                                                embed.setTimestamp()
                                                embed.addField("Error Code","Collector timed out")
                                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                                await msg.reply({embed})
                                         
                                                return
                                            }
                                        });
                                    } else {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Add)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","GroupId already being used")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                      
                                        return
                                    }
                                });
                                collector2.on('end', async (collection) => {
                                    if (collection.size === 0) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (Subgroups - Add)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code","Collector timed out")
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                   
                                        return
                                    }
                                });
                            } else {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (Subgroups - Add)")
                                embed.setColor(0xCC0000)
                                embed.setDescription("ERROR DETECTED")
                                embed.setTimestamp()
                                embed.addField("Error Code","Invalid option given")
                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                await msg.reply({embed})
                          
                                return
                            }
                        });
                        collector.on('end', async (collection) => {
                            if (collection.size === 0) {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (Subgroups)")
                                embed.setColor(0xCC0000)
                                embed.setDescription("ERROR DETECTED")
                                embed.setTimestamp()
                                embed.addField("Error Code","Collector timed out")
                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                await msg.reply({embed})
                              
                                return
                            }
                        });
                        }
                        
                    }
                });
            };
            if (params[0].toLowerCase() === "groupid") {
                db.collection("Guild_Data").findOne({
                    _id: msg.guild.id
                }, async function(err, result) {
                    if (result === null) {
                        let embed = new Discord.RichEmbed()
                        embed.setTitle("MercApi - Guild setup (GroupId)")
                        embed.setColor(0xCC0000)
                        embed.setDescription("ERROR DETECTED")
                        embed.setTimestamp()
                        embed.addField("Error Code","Guild not setup")
                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                        await msg.reply({embed})
                        
                        return
                    } else {
                        msg.reply("Please state a valid **GroupID**");
                        const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 20000, max: 1 });
                        collector.on('collect', async (message) => {
                            let id = parseInt(message.content, 10);
                            if (isNaN(id)) {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (GroupId - Edit)")
                                embed.setColor(0xCC0000)
                                embed.setDescription("ERROR DETECTED")
                                embed.setTimestamp()
                                embed.addField("Error Code","Invalid number specified")
                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                await msg.reply({embed})
                               
                                return
                            }
                                var Old_GroupID = {
                                    _id: msg.guild.id,
                                    groupId: result.groupId
                                };
                                var New_GroupID = {
                                    $set: {
                                        groupId: id
                                    }
                                };
                                db.collection("Guild_Data").updateOne(Old_GroupID, New_GroupID, async function(err3, res) {
                                    if (err3) {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (GroupId - Edit)")
                                        embed.setColor(0xCC0000)
                                        embed.setDescription("ERROR DETECTED")
                                        embed.setTimestamp()
                                        embed.addField("Error Code",err3)
                                        embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                        await msg.reply({embed})
                                       
                                        return
                                    } else {
                                        let embed = new Discord.RichEmbed()
                                        embed.setTitle("MercApi - Guild setup (GroupId - Edit)")
                                        embed.setColor(0x00AE86)
                                        embed.setDescription("Successfully set the GroupID from `" + result.groupId + "` to `" + id + "`!")
                                        embed.setTimestamp()
                                        embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                        await msg.reply({embed})
                                        return
                                    }
                                   
                                });

                        });
                        collector.on('end', async (collection) => {
                            if (collection.size === 0) {
                                let embed = new Discord.RichEmbed()
                                embed.setTitle("MercApi - Guild setup (GroupId)")
                                embed.setColor(0xCC0000)
                                embed.setDescription("ERROR DETECTED")
                                embed.setTimestamp()
                                embed.addField("Error Code","Collector timed out")
                                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                await msg.reply({embed})
                                
                                return
                            }
                        });
                    }
                });
            }
            //client.close()
        });
};

module.exports.conf = {
    enabled: true, // not used yet
    guildOnly: true, // not used yet
    permLevel: 3 // Permissions Required, higher is more power
};

module.exports.help = {
    name: "set",
    description: "Test",
    usage: "set [arg] [arg]"
};