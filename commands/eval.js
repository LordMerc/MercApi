module.exports.conf = {
  enabled: true,
  guildOnly: false,
  permLevel: 4
};
/* eslint-disable no-unused-vars */

const beautify = require('js-beautify').js_beautify;
const Discord = require('discord.js');

module.exports.run = (bot, msg, args) => {
    const parsed = bot.utils.parseArgs(args, ['e', 'nb', 'h', 'nd']);

    if (parsed.leftover.length < 1)
        throw 'You must provide a command to run!';

    if (parsed.options.e && msg.guild)
        bot.utils.assertEmbedPermission(msg.channel, msg.member);

    const input = parsed.leftover.join(' ');

    let beginTime;
    new Promise(resolve => {
        new Promise(resolve => {
            beginTime = process.hrtime();
            resolve(eval(input));
        }).then(output =>
            resolve({ output, error: false })
        ).catch(error =>
            resolve({ output: null, error })
        );
    }).then(result => {
        const elapsedTime = process.hrtime(beginTime);
        const elapsedTimeNs = elapsedTime[0] * 1e9 + elapsedTime[1];

        let fallback = '';
        if (result.error) {
            console.error(`Evaluation error:\n${(result.error.stack || result.error).toString()}`);
        } else if (result.output !== undefined && result.output !== null) {
            fallback += 'Output too long ';
            switch (result.output.constructor.name) {
            case 'Object':
                fallback += `(Object keys: ${Object.keys(result.output).length})`;
                break;
            case 'Map':
            case 'Collection':
                fallback += `(${result.output.constructor.name} size: ${result.output.size})`;
                break;
            case 'Array':
            case 'String':
                fallback += `(${result.output.constructor.name} length: ${result.output.length})`;
                break;
            default:
                fallback += parsed.options.h ? '(check Hastebin)' : '(consider using -h option to upload output to Hastebin)';
            }
            fallback += '.';
        }

        let disout = result.error ? result.error.toString() : require('util').inspect(result.output, { depth: parsed.options.nd ? 2 : 0 });
        // NOTE: Replace token
        disout = disout.replace(new RegExp(`${bot.token.split('').join('[^]{0,2}')}|${bot.token.split('').reverse().join('[^]{0,2}')}`, 'g'), '<Token>');
        // NOTE: Replace path
        // NOTE: Replace quote
     //   disout = disout.replace(new RegExp(`${bot.roblox.information['password']}`, 'g'), '<Password>');
        disout = disout.replace(new RegExp('`', 'g'), '\\`');

        new Promise(resolve => {
            if (parsed.options.h)
                resolve(bot.utils.haste(disout, 'js'));
            else
                resolve();
        }).then(haste => {
            const timeTaken = elapsedTimeNs < 1e9 ? `${(elapsedTimeNs / 1e6).toFixed(3)} ms` : `${(elapsedTimeNs / 1e9).toFixed(3)} s`;

            const fields = [
                {
                    title: 'Input',
                    fields: [
                        {
                            value: parsed.options.nb ? input : beautify(input)
                        }
                    ]
                },
                {
                    icon: result.error ? '🕱' : undefined,
                    title: result.error ? 'Error' : 'Output',
                    fields: [
                        {
                            value: disout,
                            alt: fallback
                        }
                    ]
                }
            ];

            if (haste)
                fields.push({
                    title: 'Hastebin',
                    fields: [
                        {
                            value: haste
                        }
                    ]
                });

            const formatted = bot.utils.formatEmbed('', '', fields,
                {
                    footer: `${result.output != null ? `Type: ${result.output.constructor.name} | `: ''}Node.js - Execution time: ${timeTaken}`,
                    footerIcon: 'https://a.safe.moe/UBEUl.png',
                    color: '#6cc24a',
                    code: 'js'
                }
            );

            if (typeof formatted == 'string')
                msg.channel.send(formatted);
            else
                msg.channel.send({ embed: formatted });
        });
    });
};

module.exports.help = {
  name: 'eval',
  description: 'Evaluates arbitrary javascript.',
  usage: 'eval [...code]'
};
