var MongoClient = require('mongodb').MongoClient;
var Discord = require("discord.js");
var rbx = require('roblox-js');
var url = "";
module.exports.run = async (bot, msg, params = []) => {
if (msg.mentions.members.size === 0) {
    let embed = new Discord.RichEmbed()
    embed.setTitle("MercApi - Unverified User Data")
    embed.setColor(0xCC0000)
    embed.setDescription("ERROR DETECTED")
    embed.setTimestamp()
    embed.addField("Error Code","No mentioned user")
    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
    await msg.reply({embed})
    return
} else {
    var User = msg.mentions.users.array()[0];
    var member = msg.guild.member(User);
    MongoClient.connect(url, function(err, client) {
        const db = client.db("MercApi")
        db.collection("Verification_Data").findOne({_id: member.user.id}, async function(err, result) {
            if (result === null) {
                let embed = new Discord.RichEmbed()
                embed.setTitle("MercApi - Unverified User Data")
                embed.setColor(0xCC0000)
                embed.setDescription("ERROR DETECTED")
                embed.setTimestamp()
                embed.addField("Error Code","User is not verified")
                embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                await msg.reply({embed})
                client.close()
                return
            } else {
                var name = await rbx.getUsernameFromId(parseInt(result.userid))
                let embed = new Discord.RichEmbed()
                embed.setTitle("MercApi - Verified User Data")
                embed.setColor(0x00AE86)
                embed.setDescription(`That user is [${name}](https://www.roblox.com/users/${result.userid}/profile)`)
                embed.setTimestamp()
                embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                await msg.reply({embed})
                client.close()
                return
            }
        });
});
}
};

module.exports.conf = {
    enabled: true, // not used yet
    guildOnly: true, // not used yet
    permLevel: 0 // Permissions Required, higher is more power
};

module.exports.help = {
    name: "whois",
    description: "Gets mentioned username",
    usage: "whois mention"
};