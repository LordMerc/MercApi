const Discord = require("discord.js")
module.exports.run = (bot, msg, params) => {
  let command;
  if (bot.commands.has(params[0])) {
    command = params[0];
  }
  if (!command) {
      let embed = new Discord.RichEmbed()
      .setColor("RED")
      .setDescription(`Failed to find command **${params[0]}**`)
      return msg.channel.send({embed})
  } else {
    let embed = new Discord.RichEmbed()
    .setColor("RANDOM")
    .setDescription(`Reloading **${command[0]}**`)
    msg.channel.send({embed}).then(m => {
      bot.reload(command)
      .then(() => {
        m.delete();
        let embed2 = new Discord.RichEmbed()
        .setColor("GREEN")
        .setDescription(`Successfully reloaded **${command[0]}**`)
        return msg.channel.send({embed2})
      })
      .catch(e => {
        m.delete();
        let embed = new Discord.RichEmbed()
        .setColor("RED")
        .setDescription(`Command reload failed: ${command}\n\`\`\`${e.stack}\`\`\``)
        return msg.channel.send({embed})
      });
    });
  }
};

module.exports.conf = {
  enabled: true,
  guildOnly: false,
  permLevel: 4
};

module.exports.help = {
  name: "reload",
  description: "Reloads the command file, if it's been updated or modified.",
  usage: "reload <commandname>"
};
``
