var MongoClient = require('mongodb').MongoClient;
var Discord = require("discord.js");
var url = "";
module.exports.run = (bot, msg, params = []) => {
    MongoClient.connect(url, function(err, client) {
        const db = client.db("MercApi")
        if (err) {
            console.log("Failed (" + err + ")");
            client.close()
            return false;
        }
        db.collection("Verification_Data").findOne({_id: msg.author.id}, async function(err, result) {
        if (result === null) {
            let embed = new Discord.RichEmbed()
            embed.setTitle("MercApi - Unlinking")
            embed.setColor(0xCC0000)
            embed.setDescription("ERROR DETECTED")
            embed.setTimestamp()
            embed.addField("Error Code","User not verified")
            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
            await msg.channel.send({embed})
            client.close()
            return;
        } else {
            msg.reply("Are you sure you want to unverify? Please reply **Yes** or **No**");
            const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1});
            collector.on('collect', async (message) => {
                if (message.content === "Yes" || message.content === "yes") {
                    db.collection("Verification_Data").deleteOne({_id: msg.author.id}, async function(err, result) {
                    if (err) throw err;
                        if (msg.member.roles.find('name',"Verified")) {
                            await msg.member.removeRole(msg.guild.roles.find("name","Verified"))
                        }
                        let embed = new Discord.RichEmbed()
                        embed.setTitle("MercApi - Unlinking")
                        embed.setColor(0x00AE86)
                        embed.setDescription(`Successfully unlinked, you may re-verify in the future!`)
                        embed.setTimestamp()
                        embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                        await msg.channel.send({embed})
                        client.close()
                        return;
                    })
                } else if (message.content.toLowerCase() === "no") {
                    await msg.reply("Successfully cancelled prompt")
                    client.close()
                    return
                }
                else {
                    let embed = new Discord.RichEmbed()
                    embed.setTitle("MercApi - Unlinking")
                    embed.setColor(0xCC0000)
                    embed.setDescription("ERROR DETECTED")
                    embed.setTimestamp()
                    embed.addField("Error Code","Invalid option detected")
                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                    await msg.channel.send({embed})
                    client.close()
                    return;
                }
            })
            collector.on('end', async (collection) => {
                if (collection.size === 0) {
                    let embed = new Discord.RichEmbed()
                    embed.setTitle("MercApi - Unlinking")
                    embed.setColor(0xCC0000)
                    embed.setDescription("ERROR DETECTED")
                    embed.setTimestamp()
                    embed.addField("Error Code","Collector timed out")
                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                    await msg.channel.send({embed})
                    client.close()
                    return;
                }
            });
        }
        });
    });
};

module.exports.conf = {
    enabled: true, // not used yet
    guildOnly: true, // not used yet
    permLevel: 0 // Permissions Required, higher is more power
};

module.exports.help = {
    name: "unlink",
    description: "Remove verification",
    usage: "unlink"
};