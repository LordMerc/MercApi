module.exports.run = (bot, msg, params = []) => {
  msg.channel.send({embed: {
    color: 3447003,
    description: params.join(" ")
  }});
};

module.exports.conf = {
  enabled: true, // not used yet
  guildOnly: false, // not used yet
  permLevel: 0 // Permissions Required, higher is more power
};

module.exports.help = {
  name : "say",
  description: "Bot says what you say",
  usage: "say <argument>"
};
