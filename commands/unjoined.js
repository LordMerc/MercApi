let Discord = require("discord.js")
module.exports.run = async (bot, msg, params = []) => {
    let Army = 3639121
    let Army_Ranks = [
        {name:"E6 (Not Required)",rankid:7},
        {name:"01 (Required)",rankid:9},
        {name:"02 (Required)",rankid:10},
        {name:"03 (Required)",rankid:11},
        {name:"04 (Required)",rankid:12},
        {name:"05 (Required)",rankid:13},
        {name:"06 (Required)",rankid:14},
    ]
    let Academy = 3643718
    let Navy = 3024818
    let Navy_Ranks = [
        {name:"E6 (Not Required)",rankid:6},
        {name:"01 (Required)",rankid:8},
        {name:"02 (Required)",rankid:9},
        {name:"03 (Required)" ,rankid:10},
        {name:"04 (Required)",rankid:11},
        {name:"05 (Required)",rankid:12},
    ]
    if (params[0].toLowerCase() === "army") {
        let unjoined = []
        msg.reply("Initiated request")
        new Promise(async function(r,rj){
            for (let Selection of Army_Ranks) {
                let Army_People = await bot.rbx.getPlayers({group:Army,rank:Selection.rankid})
            for (let Player of Army_People.players) {
                let PId = await bot.rbx.getIdFromUsername(Player.name.toLowerCase())
                let Army_Rank = await bot.rbx.getRankNameInGroup(Academy,PId)
                if (Army_Rank === "Guest") {
                    unjoined.push(Player.name + ` ***(${Selection.name})***`)
                }
            }
        }
            console.log(unjoined)
            r()
        }).then(function(){
            let embed = new Discord.RichEmbed()
            embed.setAuthor("MercApi v2","https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png") 
            embed.setColor(0x00AE86)
            embed.setTitle(`Unjoined OA personnel above E6-ARMY`)
            embed.setDescription(`Total: **${unjoined.length}**`)
            if (unjoined.length > 0) {
                embed.addField("Unjoined Personnel",unjoined.map(p => p).join("\n"))
            }
            return msg.reply({embed})
        })
    } else if (params[0].toLowerCase() === "navy") {
        let unjoined = []
        msg.reply("Initiated request")
        new Promise(async function(r,rj){
            for (let Selection of Navy_Ranks) {
                let Navy_People = await bot.rbx.getPlayers({group:Navy,rank:Selection.rankid})
            for (let Player of Navy_People.players) {
                let PId = await bot.rbx.getIdFromUsername(Player.name.toLowerCase())
                let Navy_Rank = await bot.rbx.getRankNameInGroup(Academy,PId)
                if (Navy_Rank === "Guest") {
                    unjoined.push(Player.name + ` ***(${Selection.name})***`)
                }
            }
        }
            console.log(unjoined)
            r()
        }).then(function(){
            let embed = new Discord.RichEmbed()
            embed.setAuthor("MercApi v2","https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png") 
            embed.setColor(0x00AE86)
            embed.setTitle(`Unjoined OA personnel above E6-NAVY`)
            embed.setDescription(`Total: **${unjoined.length}**`)
            if (unjoined.length > 0) {
                embed.addField("Unjoined Personnel",unjoined.map(p => p).join("\n"))
            }
            return msg.reply({embed})
        })
    }
  };
  
  module.exports.conf = {
    enabled: true, // not used yet
    guildOnly: false, // not used yet
    permLevel: 0 // Permissions Required, higher is more power
  };
  
  module.exports.help = {
    name : "unjoined",
    description: "Bot says what you say",
    usage: "say <argument>"
  };
  