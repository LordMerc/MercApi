var MongoClient = require('mongodb').MongoClient;
var Discord = require("discord.js");
var crypto = require('crypto');
var rbx = require('roblox-js');
var lodash = require('lodash')
const safe_words = ['cats love tacos','random argument here','sports player','soccer is the best sport','dogs are better than cats']
var url = "";
function login () {
    return rbx.login("", "");
  }
  async function randomValueHex (len) {
    return crypto.randomBytes(Math.ceil(len/2))
        .toString('hex') // convert to hexadecimal format
        .slice(0,len);   // return required number of characters
}
const grabprefix = async(msg) => {
    let client = await MongoClient.connect(url)
    const db = client.db("MercApi")
    let result = await db.collection("Guild_Data").findOne({_id: msg.guild.id})
    return result
    client.close()
}
module.exports.run = (bot, msg, params = []) => {
    MongoClient.connect(url, function(err, client) {
        const db = client.db("MercApi")
        if (err) {
            console.log("Failed (" + err + ")");
            return false;
        }
       
        db.collection("Verification_Data").findOne({_id: msg.author.id}, async function(err, result) {
           if (result) {
               if (msg.member.roles.find("name","Verifying")) {
                await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
               }
               if (!msg.member.roles.find("name","Verified")) {
                await msg.member.addRole(msg.guild.roles.find("name","Verified"))
               }
            let embed = new Discord.RichEmbed()
            embed.setTitle("MercApi - Verification")
            embed.setColor(0xCC0000)
            embed.setDescription("ERROR DETECTED")
            embed.setTimestamp()
            embed.addField("Error Code","User is already verified")
            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
            await msg.channel.send({embed})
            return
           }
           try {
            msg.reply("Please reply with a valid **Username** or a valid **UserId**")
            let collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1})
            collector.on('collect', async(message1) => {
                let response = await message1.content
                let Username
                let UserId
                try {
                    let IdTry = await rbx.getUsernameFromId(response)
                    Username = IdTry
                    let NameTry = await rbx.getIdFromUsername(IdTry)
                    UserId = NameTry
                } catch(err) {
                    try {
                        let NameTry = await rbx.getIdFromUsername(response)
                        UserId = NameTry
                        let IdTry = await rbx.getUsernameFromId(UserId)
                        Username = IdTry
                    } catch (err) {
                         // /console.log(err)
                    }
                }
                if (Username && UserId) {
                    msg.reply("`You have chosen to verify as: " + Username + " with UserId: " + UserId + "`")
                    msg.reply("`Please choose an option to verify with:` **Status** or **Blurb**")
                    let collector1 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1})
                    collector1.on('collect', async(message2) => {
                        if (message2.content === "Status" || message2.content === "status") {
                            msg.reply("Is your ROBLOX account **UNDER** the age of 13 years old? Reply **Yes** or **No**")
                            let collector2 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1})
                            collector2.on('collect', async(message3) => {
                                if (message3.content === "Yes" || message3.content === "yes") {
                                    let passcode = lodash.sample(safe_words)
                                    msg.reply("Please insert the following code into your status: `" + passcode + "`")
                                    msg.reply("Reply when you have finished")
                                    let collector3 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1})
                                    collector3.on('collect', async(message3) => {
                                        let status = await rbx.getStatus(UserId)
                                        if (status.indexOf(passcode) > -1) {
                                            var myobj = {
                                                _id: msg.author.id,
                                                userid: UserId,
                                              };
                                              db.collection("Verification_Data").insertOne(myobj, async function(err, ress) {
                                                if (err) console.log(err);
                                                try {
                                                    await msg.member.setNickname(Username,"Verified");
                                                    await msg.member.addRole(msg.guild.roles.find("name","Verified"))
                                                    if (msg.member.roles.find("name","Verifying")) {
                                                        await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
                                                    }
                                                    let gprefix = await grabprefix(msg)
                                                    let prefix = (gprefix.prefix && gprefix.prefix || "-")
                                                    let embed = new Discord.RichEmbed()
                                                    embed.setTitle("MercApi - Verification (Succeeded - Verified given and nicknamed)")
                                                    embed.setColor(0x00AE86)
                                                    embed.setDescription(`Successfully Verified, **${Username}**!\nYou may now run ${prefix}getrole to receive your roles!`)
                                                    embed.setTimestamp()
                                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                    await msg.channel.send({embed})
                                                    client.close()
                                                    return;
                                                } catch (err) {
                                                    await msg.member.addRole(msg.guild.roles.find("name","Verified"))
                                                    if (msg.member.roles.find("name","Verifying")) {
                                                        await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
                                                    }
                                                    let gprefix = await grabprefix(msg)
                                                    let prefix = (gprefix.prefix && gprefix.prefix || "-")
                                                    let embed = new Discord.RichEmbed()
                                                    embed.setTitle("MercApi - Verification (Succeeded - Verified given, but not nicknamed)")
                                                    embed.setColor(0x00AE86)
                                                    embed.setDescription(`Successfully Verified, **${Username}**!\nYou may now run ${prefix}getrole to receive your roles!`)
                                                    embed.setTimestamp()
                                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                    await msg.channel.send({embed})
                                                    client.close()
                                                    return;
                                                }
                                              })
                                        } else {
                                            let embed = new Discord.RichEmbed()
                                            embed.setTitle("MercApi - Verification (Failed)")
                                            embed.setColor(0xCC0000)
                                            embed.setDescription("ERROR DETECTED")
                                            embed.setTimestamp()
                                            embed.addField("Error Code","Could not find code in status")
                                            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                            await msg.channel.send({embed})
                                            client.close()
                                            return;
                                        }
                                    })
                                    collector3.on('end', async(collection) => {
                                        if (collection.size === 0) {
                                            let embed = new Discord.RichEmbed()
                                            embed.setTitle("MercApi - Verification")
                                            embed.setColor(0xCC0000)
                                            embed.setDescription("ERROR DETECTED")
                                            embed.setTimestamp()
                                            embed.addField("Error Code","Collector timed out")
                                            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                            await msg.channel.send({embed})
                                            client.close()
                                            return;
                                        }
                                    })

                                } else if (message3.content === "No" || message3.content === "no") {
                                    let passcode = await randomValueHex(12)
                                    msg.reply("Please insert the following code into your status: `" + passcode + "`")
                                    msg.reply("Reply when you have finished")
                                    let collector3 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1})
                                    collector3.on('collect', async(message3) => {
                                        let status = await rbx.getStatus(UserId)
                                        if (status.indexOf(passcode) > -1) {
                                            var myobj = {
                                                _id: msg.author.id,
                                                userid: UserId,
                                              };
                                              db.collection("Verification_Data").insertOne(myobj, async function(err, ress) {
                                                if (err) console.log(err);
                                                try {
                                                    await msg.member.setNickname(Username,"Verified");
                                                    await msg.member.addRole(msg.guild.roles.find("name","Verified"))
                                                    if (msg.member.roles.find("name","Verifying")) {
                                                        await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
                                                    }
                                                    let gprefix = await grabprefix(msg)
                                                    let prefix = (gprefix.prefix && gprefix.prefix || "-")
                                                    let embed = new Discord.RichEmbed()
                                                    embed.setTitle("MercApi - Verification (Succeeded - Verified given and nicknamed)")
                                                    embed.setColor(0x00AE86)
                                                    embed.setDescription(`Successfully Verified, **${Username}**!\nYou may now run ${prefix}getrole to receive your roles!`)
                                                    embed.setTimestamp()
                                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                    await msg.channel.send({embed})
                                                    client.close()
                                                    return;
                                                } catch (err) {
                                                    await msg.member.addRole(msg.guild.roles.find("name","Verified"))
                                                    if (msg.member.roles.find("name","Verifying")) {
                                                        await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
                                                    }
                                                    let gprefix = await grabprefix(msg)
                                                    let prefix = (gprefix.prefix && gprefix.prefix || "-")
                                                    let embed = new Discord.RichEmbed()
                                                    embed.setTitle("MercApi - Verification (Succeeded - Verified given, but not nicknamed)")
                                                    embed.setColor(0x00AE86)
                                                    embed.setDescription(`Successfully Verified, **${Username}**!\nYou may now run ${prefix}getrole to receive your roles!`)
                                                    embed.setTimestamp()
                                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                    await msg.channel.send({embed})
                                                    client.close()
                                                    return;
                                                }
                                              })
                                        } else {
                                            let embed = new Discord.RichEmbed()
                                            embed.setTitle("MercApi - Verification (Failed)")
                                            embed.setColor(0xCC0000)
                                            embed.setDescription("ERROR DETECTED")
                                            embed.setTimestamp()
                                            embed.addField("Error Code","Could not find code in status")
                                            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                            await msg.channel.send({embed})
                                            client.close()
                                            return
                                        }
                                    })
                                } else {
                                    let embed = new Discord.RichEmbed()
                                    embed.setTitle("MercApi - Verification")
                                    embed.setColor(0xCC0000)
                                    embed.setDescription("ERROR DETECTED")
                                    embed.setTimestamp()
                                    embed.addField("Error Code","Invalid option detected")
                                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                    await msg.channel.send({embed})
                                    client.close()
                                    return;
                                }
                            })
                            collector2.on('end', async(collection) => {
                                if (collection.size === 0) {
                                    let embed = new Discord.RichEmbed()
                                    embed.setTitle("MercApi - Verification")
                                    embed.setColor(0xCC0000)
                                    embed.setDescription("ERROR DETECTED")
                                    embed.setTimestamp()
                                    embed.addField("Error Code","Collector timed out")
                                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                    await msg.channel.send({embed})
                                    client.close()
                                    return;
                                }
                            })
                        } else if (message2.content === "Blurb" || message2.content === "blurb") {
                            msg.reply("Is your ROBLOX account **UNDER** the age of 13 years old? Reply **Yes** or **No**")
                            let collector2 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1})
                            collector2.on('collect', async(message3) => {
                                if (message3.content === "Yes" || message3.content === "yes") {
                                    let passcode = lodash.sample(safe_words)
                                    msg.reply("Please insert the following code into your blurb: `" + passcode + "`")
                                    msg.reply("Reply when you have finished")
                                    let collector3 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1})
                                    collector3.on('collect', async(message3) => {
                                        let bio = await rbx.getBlurb(UserId)
                                        if (bio.indexOf(passcode) > -1) {
                                            var myobj = {
                                                _id: msg.author.id,
                                                userid: UserId,
                                              };
                                              db.collection("Verification_Data").insertOne(myobj, async function(err, ress) {
                                                if (err) console.log(err);
                                                try {
                                                    await msg.member.setNickname(Username,"Verified");
                                                    await msg.member.addRole(msg.guild.roles.find("name","Verified"))
                                                    if (msg.member.roles.find("name","Verifying")) {
                                                        await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
                                                    }
                                                    let gprefix = await grabprefix(msg)
                                                    let prefix = (gprefix.prefix && gprefix.prefix || "-")
                                                    let embed = new Discord.RichEmbed()
                                                    embed.setTitle("MercApi - Verification (Succeeded - Verified given and nicknamed)")
                                                    embed.setColor(0x00AE86)
                                                    embed.setDescription(`Successfully Verified, **${Username}**!\nYou may now run ${prefix}getrole to receive your roles!`)
                                                    embed.setTimestamp()
                                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                    await msg.channel.send({embed})
                                                    client.close()
                                                    return;
                                                } catch (err) {
                                                    await msg.member.addRole(msg.guild.roles.find("name","Verified"))
                                                    if (msg.member.roles.find("name","Verifying")) {
                                                        await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
                                                    }
                                                    let gprefix = await grabprefix(msg)
                                                    let prefix = (gprefix.prefix && gprefix.prefix || "-")
                                                    let embed = new Discord.RichEmbed()
                                                    embed.setTitle("MercApi - Verification (Succeeded - Verified given, but not nicknamed)")
                                                    embed.setColor(0x00AE86)
                                                    embed.setDescription(`Successfully Verified, **${Username}**!\nYou may now run ${prefix}getrole to receive your roles!`)
                                                    embed.setTimestamp()
                                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                    await msg.channel.send({embed})
                                                    client.close()
                                                    return;
                                                }
                                              })
                                        } else {
                                            let embed = new Discord.RichEmbed()
                                            embed.setTitle("MercApi - Verification (Failed)")
                                            embed.setColor(0xCC0000)
                                            embed.setDescription("ERROR DETECTED")
                                            embed.setTimestamp()
                                            embed.addField("Error Code","Could not find code in blurb")
                                            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                            await msg.channel.send({embed})
                                            client.close()
                                            return;
                                        }
                                    })
                                    collector3.on('end', async(collection) => {
                                        if (collection.size === 0) {
                                            let embed = new Discord.RichEmbed()
                                            embed.setTitle("MercApi - Verification")
                                            embed.setColor(0xCC0000)
                                            embed.setDescription("ERROR DETECTED")
                                            embed.setTimestamp()
                                            embed.addField("Error Code","Collector timed out")
                                            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                            await msg.channel.send({embed})
                                            client.close()
                                            return;
                                        }
                                    })

                                } else if (message3.content === "No" || message3.content === "no") {
                                    let passcode = await randomValueHex(12)
                                    msg.reply("Please insert the following code into your blurb: `" + passcode + "`")
                                    msg.reply("Reply when you have finished")
                                    let collector3 = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 120000, max: 1})
                                    collector3.on('collect', async(message3) => {
                                        let bio = await rbx.getBlurb(UserId)
                                        if (bio.indexOf(passcode) > -1) {
                                            var myobj = {
                                                _id: msg.author.id,
                                                userid: UserId,
                                              };
                                              db.collection("Verification_Data").insertOne(myobj, async function(err, ress) {
                                                if (err) console.log(err);
                                                try {
                                                    await msg.member.setNickname(Username,"Verified");
                                                    await msg.member.addRole(msg.guild.roles.find("name","Verified"))
                                                    if (msg.member.roles.find("name","Verifying")) {
                                                        await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
                                                    }
                                                    let gprefix = await grabprefix(msg)
                                                    let prefix = (gprefix.prefix && gprefix.prefix || "-")
                                                    let embed = new Discord.RichEmbed()
                                                    embed.setTitle("MercApi - Verification (Succeeded - Verified given and nicknamed)")
                                                    embed.setColor(0x00AE86)
                                                    embed.setDescription(`Successfully Verified, **${Username}**!\nYou may now run ${prefix}getrole to receive your roles!`)
                                                    embed.setTimestamp()
                                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                    await msg.channel.send({embed})
                                                    client.close()
                                                    return;
                                                } catch (err) {
                                                    await msg.member.addRole(msg.guild.roles.find("name","Verified"))
                                                    if (msg.member.roles.find("name","Verifying")) {
                                                        await msg.member.removeRole(msg.guild.roles.find("name","Verifying")) 
                                                    }
                                                    let gprefix = await grabprefix(msg)
                                                    let prefix = (gprefix.prefix && gprefix.prefix || "-")
                                                    let embed = new Discord.RichEmbed()
                                                    embed.setTitle("MercApi - Verification (Succeeded - Verified given, but not nicknamed)")
                                                    embed.setColor(0x00AE86)
                                                    embed.setDescription(`Successfully Verified, **${Username}**!\nYou may now run ${prefix}getrole to receive your roles!`)
                                                    embed.setTimestamp()
                                                    embed.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f1/CheckE.png")
                                                    await msg.channel.send({embed})
                                                    client.close()
                                                    return;
                                                }
                                              })
                                        } else {
                                            let embed = new Discord.RichEmbed()
                                            embed.setTitle("MercApi - Verification (Failed)")
                                            embed.setColor(0xCC0000)
                                            embed.setDescription("ERROR DETECTED")
                                            embed.setTimestamp()
                                            embed.addField("Error Code","Could not find code in blurb")
                                            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                            await msg.channel.send({embed})
                                            client.close()
                                            return
                                        }
                                    })
                                } else {
                                    let embed = new Discord.RichEmbed()
                                    embed.setTitle("MercApi - Verification")
                                    embed.setColor(0xCC0000)
                                    embed.setDescription("ERROR DETECTED")
                                    embed.setTimestamp()
                                    embed.addField("Error Code","Invalid option detected")
                                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                    await msg.channel.send({embed})
                                    client.close()
                                    return;
                                }
                            })
                            collector2.on('end', async(collection) => {
                                if (collection.size === 0) {
                                    let embed = new Discord.RichEmbed()
                                    embed.setTitle("MercApi - Verification")
                                    embed.setColor(0xCC0000)
                                    embed.setDescription("ERROR DETECTED")
                                    embed.setTimestamp()
                                    embed.addField("Error Code",err)
                                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                                    await msg.channel.send({embed})
                                    client.close()
                                    return;
                                }
                            })
                        } else {
                            let embed = new Discord.RichEmbed()
                            embed.setTitle("MercApi")
                            embed.setColor(0xCC0000)
                            embed.setDescription("ERROR DETECTED")
                            embed.setTimestamp()
                            embed.addField("Error Code","Invalid option detected")
                            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                            await msg.channel.send({embed})
                            client.close()
                            return;
                        }
                    })
                    collector1.on('end', async(collection) => {
                        if (collection.size === 0) {
                            let embed = new Discord.RichEmbed()
                            embed.setTitle("MercApi - Verification")
                            embed.setColor(0xCC0000)
                            embed.setDescription("ERROR DETECTED")
                            embed.setTimestamp()
                            embed.addField("Error Code",err)
                            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                            await msg.channel.send({embed})
                            client.close()
                            return;
                        }
                    })
                } else {
                    let embed = new Discord.RichEmbed()
                    embed.setTitle("MercApi - Verification")
                    embed.setColor(0xCC0000)
                    embed.setDescription("ERROR DETECTED")
                    embed.setTimestamp()
                    embed.addField("Error Code","Invalid Username/UserId supplied.")
                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                    await msg.channel.send({embed})
                    client.close()
                    return;
                }
            })
            collector.on('end', async(collection) => {
                if (collection.size === 0) {
                    let embed = new Discord.RichEmbed()
                    embed.setTitle("MercApi - Verification")
                    embed.setColor(0xCC0000)
                    embed.setDescription("ERROR DETECTED")
                    embed.setTimestamp()
                    embed.addField("Error Code","Collector timed out")
                    embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
                    await msg.channel.send({embed})
                    client.close()
                    return;
                }
            })
           } catch(err) {
            let embed = new Discord.RichEmbed()
            embed.setTitle("MercApi - Verification")
            embed.setColor(0xCC0000)
            embed.setDescription("ERROR DETECTED")
            embed.setTimestamp()
            embed.addField("Error Code",err)
            embed.setThumbnail("http://www.freeiconspng.com/uploads/x-png-23.png")
            await msg.channel.send({embed})
            client.close()
           }
        });
    });
};

module.exports.conf = {
    enabled: true, // not used yet
    guildOnly: true, // not used yet
    permLevel: 0 // Permissions Required, higher is more power
};

module.exports.help = {
    name: "verify",
    description: "Verify ROBLOX to Discord",
    usage: "verify"
};

