const request = require('request');
const jar = request.jar();
const rbx = require('roblox-js');

module.exports.run = (bot, msg, params = []) => {
    let suffix = params[0];
  rbx.login(bot.roblox.information['username'], bot.roblox.information['password']).then(function () {
    rbx.getIdFromUsername(suffix).then(function(ID){
      request("https://mercapi.herokuapp.com/getInfo/" + ID, (error, result, body) => {
        var returned = JSON.parse(body);
          //begining
          if (returned.message === "Failure!") {
            bot.sendError(msg,"Unable to find user");
            return;
          }
          msg.channel.send({embed: {
            color: 3447003,
            title: "Information on " + returned.data.name,
            author: { name: bot.user.username, icon_url: bot.user.displayAvatarURL },
            description: "Successfully retrieved information",
            url: 'https://www.roblox.com/users/' + ID,
            thumbnail: {url: returned.data.picture},

            fields: [
        {
          name: 'Friends',
          value: returned.data.friends,
          inline: true
        },
        {
          name: 'Status',
          value: returned.data.status === "" ? "None" : returned.data.status,
          inline: true
        },
        {
          name: 'Blurb',
          value: returned.data.blurb === "" ? "None" : returned.data.blurb,
          inline: true
        },
        {
          name: 'Place Visits',
          value: returned.data.place_visits,
          inline: true
        },
        {
          name: 'Join Date',
          value: returned.data.join_date,
          inline: true
        },
        {
          name: 'Groups',
          value: returned.data.groups.length === 0 ? "None Detected" :  returned.data.groups.splice(-5).map(c=>`[${c.Name}](https://www.roblox.com/My/Groups.aspx?gid=${c.Id}) : **${c.Role}**`).join("\n"),
          inline: true
        }
      ],
        }})
          //end
    }); //END OF request
  }).catch(function(err){
    bot.sendError(msg,"Failure: " + err);
    return;
  })
});
};

module.exports.conf = {
  enabled: true, // not used yet
  guildOnly: false, // not used yet
  permLevel: 4 // Permissions Required, higher is more power
};

module.exports.help = {
  name : "getinfo",
  description: "Get ROBLOX player information",
  usage: "getinfo <argument>"
};