_promise = function(_func) {
  return new Promise(function(_resolve, _reject) {
      return _func(_resolve, _reject)
  });
}
// ok
const express = require('express')
var validator = require('validator');
const rbx = require('roblox-js')
const fs = require('fs')
const bodyParser = require('body-parser')
const settings = require('./settings.json')
const Config = require('./config.json')
const rbx2 = require('./roblox-js-v2')
const http = require("http")
const ejs = require('ejs')
const Promise = require('bluebird')
const events = require("events")
const Rbx_Ready = new events.EventEmitter()
const key = settings.key
const app = express()
const usersite = "https://www.roblox.com/users/%s/profile"
const port = process.env.PORT || 8080
const MongoClient = require('mongodb').MongoClient
const url = ""
const request = require('request-promise')
const jar = request.jar()
const Discord = require("discord.js")
const bot = global.bot = exports.client =  new Discord.Client({ fetchAllMembers: true })
bot.commands = new Discord.Collection()
bot.rbx = require('./roblox-js-v2')
// Load the contents of the `/cmd/` folder and each file in it.
fs.readdir(`./commands/`, (err, files) => {
  if(err) console.error(err);
  console.log(`Loading a total of ${files.length} commands.`);
  // Loops through each file in that folder
  files.forEach(f=> {
    // require the file itself in memory
    let props = require(`./commands/${f}`);
    console.log(`Loading Command: ${props.help.name}. :ok_hand:`);
    // add the command to the Commands Collection
    bot.commands.set(props.help.name, props);
  });
});
app.use('/api/discord', require('./api/discord'));
bot.utils = require("./util.js")
app.set('env', 'production')
app.set('views','./pages')
app.engine('html',ejs.renderFile)
app.set('view engine','html')
app.use(express.static('static'))
  process.on('unhandledRejection', function (err,p) {
    console.log(err,p)
   /* var guild = bot.guilds.get("324336831300894721")
    var channel = guild.channels.find("name","testing")
    channel.send(`
      Error Detected somehow(?): ${err.reason}
      Check logs for more info :b:
      `)
      */
})
app.use(function(err, req, res, next) {
  // Do logging and user-friendly error message display
  console.error(err)
  res.status(500).send()
})
app.use(bodyParser.json())
app.get('/', function(req,res) {
  res.render('login')
})
app.get('/GuildDatajson', function (req, res, next) {
  MongoClient.connect(url, function(err, client) {
    const db = client.db("MercApi")
    if (err) throw err;
    var mysort = { _id: 1 };
    db.collection("Guild_Data").find().sort(mysort).toArray(function(err, result) {
      if (err) throw err;
      res.json({'Message':'Success!', data: {guilds:result}});
    });
    client.close()
  }); 
});
const errHandler = function(err) {
  console.log(err);
}
bot.grabprefix = function(info) {
  return _promise(function(_resolve, _reject) {
  MongoClient.connect(url, function(err, client) {
    const db = client.db("MercApi")
    db.collection("Guild_Data").findOne({_id: info.guild.id}, function(err, result) {
     if (result) {
       _resolve(result.prefix && result.prefix || Config.prefix)
     } else {
      _resolve(Config.prefix)
     }
    });
    client.close()
  });
})
};
const rbxlogin = function(info) {
  return _promise(function(_resolve, _reject) {
    bot.rbx.login(Config.user,Config.pass).then(function() {
      _resolve()
    }).catch(
     (reason) => {
       console.log("no")
       _reject(reason)
   })
  });
}
const botlogin = function(info) {
  return _promise(function(_resolve, _reject) {
    bot.login(Config.token).then(function(){
      _resolve()
    }).catch(
     (reason) => {
       console.log("no")
       _reject(reason)
   })
  });
}
setInterval(function() {
  rbxlogin().then(function() {
    console.log("Re-logged ROBLOX")
  })
  .catch(
    (reason) => {
      console.log("Couldn't re-log ROBLOX")
  })
}, 864000);
const Logins = function() {
  console.log("Attempting Logins");
  botlogin().then(function() {
    console.log("Logged into Discord!")
    app.listen(port, function () {
      console.log('Listening')
    })
    rbxlogin().then(function() {
      console.log("Logged into ROBLOX!")
      Rbx_Ready.emit('begin')
    }).catch(
      (reason) => {
        console.log("Couldn't log into ROBLOX!")
        errHandler(reason)
    })
  }).catch(
      (reason) => {
        console.log("Couldn't log into Discord!")
        errHandler(reason)
    }
  )
}
Logins()

function validatorType (type) {
  switch (type) {
    case 'int':
      return validator.isInt;
    case 'safe_string':
      return validator.isAlphanumeric;
    case 'boolean':
      return validator.isBoolean;
    case 'string':
      return function (value) {
        return typeof value === 'string';
      };
    default:
      return function () {
        return true;
      };
  }
}

function processType (type, value) {
  switch (type) {
    case 'int':
      return parseInt(value, 10);
    case 'boolean':
      return (value === 'true');
    default:
      return value;
  }
}

function verifyParameters (res, validate, requiredFields, optionalFields) {
  var result = {};
  if (requiredFields) {
    for (var index in requiredFields) {
      var type = requiredFields[index];
      var use = validatorType(type);

      var found = false;
      for (var i = 0; i < validate.length; i++) {
        var value = validate[i][index];
        if (value) {
          if (use(value)) {
            result[index] = processType(type, value);
            found = true;
          } else {
            sendErr(res, {error: 'Parameter "' + index + '" is not the correct data type.', id: null});
            return false;
          }
          break;
        }
      }
      if (!found) {
        sendErr(res, {error: 'Parameter "' + index + '" is required.', id: null});
        return false;
      }
    }
  }
  if (optionalFields) {
    for (index in optionalFields) {
      type = optionalFields[index];
      use = validatorType(type);
      for (i = 0; i < validate.length; i++) {
        value = validate[i][index];
        if (value) {
          if (use(value)) {
            result[index] = processType(type, value);
          } else {
            sendErr(res, {error: 'Parameter "' + index + '" is not the correct data type.', id: null});
            return false;
          }
          break;
        }
      }
    }
  }
  return result;
}
////
bot.elevation = function(msg) {
  return _promise(function(_resolve, _reject) {
    /* This function should resolve to an ELEVATION level which
     is then sent to the command handler for verification*/
  let guild = bot.guilds.get("324336831300894721")
  let member = guild.members.get(msg.author.id)
  let permlvl = 0
  let mod_role = guild.roles.find("name", "Moderator")
  let admin_role = guild.roles.find("name", "Bot Administrator")
  let bot_commander = msg.guild.roles.find("name", "Bot Commander")
  if(member) {
    if(mod_role && member.roles.has(mod_role.id)) permlvl = 2
    if(admin_role && member.roles.has(admin_role.id)) permlvl = 3
    if(bot_commander && msg.member.roles.has(bot_commander.id)) permlvl = 2
  } else {
    if(bot_commander && msg.member.roles.has(bot_commander.id)) permlvl = 2
  }
  if(msg.guild.ownerID === msg.author.id) permlvl = 3
  if(msg.author.id === "80812798250070016") permlvl = 4
  _resolve(permlvl)
  })
}
bot.on('message', msg => {
  // Ignore message with no prefix for performance reasons
  if (!msg.guild) {
    return;
  }
  if (msg.author === bot.user) return
 bot.grabprefix(msg).then(function(prefix) {
  if (msg.content === "Merc prefix pls") {
    return msg.reply("Prefix is " + prefix);
  }
  if (msg.content.length === 1) {
    return;
  }
  if(!msg.content.startsWith(prefix)) return;
  // Get the command by getting the first part of the message and removing  the prefix.
  var command = msg.content.split(" ")[0].slice(prefix.length);
//  log(command);
  // Get the params in an array of arguments to be used in the bot
  var params = msg.content.split(" ").slice(1);
  //log(params);
//  var suffix = msg.content.substring(cmdTxt.length + 2);
  // run the `elevation` function to get the user's permission level
  bot.elevation(msg).then(function(perms) {
   console.log(perms)
      //log(perms);
    let cmd;
    // Check if the command exists in Commands
    if (bot.commands.has(command)) {
      // Assign the command, if it exists in Commands
      cmd = bot.commands.get(command)
    // Check if the command exists in Aliases
    }

    if(cmd) {
      // Check user's perm level against the required level in the command
      if (perms < cmd.conf.permLevel){
        msg.react('422209335129538561')
      return;
    }
    if (msg.guild.id === "408418862820884482" | msg.guild.id === "392728388000808972" && cmd.help.name != "trello") {
      return
    }
    if (cmd.conf.guildOnly === true && !msg.guild){
      msg.reply(msg,"Command is to be executed in a **server** only!");
      return;
    }
      // Run the `exports.run()` function defined in each command.
      cmd.run(bot, msg, params);
    } else {
      msg.react('422209335129538561')
    }
  })
 })
});

bot.on("error", console.error)
bot.on("guildCreate", async function(guild) {
  let Original_Channel
  if (guild.channels.first().type === "category") {
    Original_Channel = guild.channels.first().children.first()
  } else {
    Original_Channel = guild.channels.first()
  }
  MongoClient.connect(url, async function(err, client) {
    const db = client.db("MercApi")
    db.collection("Guild_Data").findOne({_id: guild.id}, async function(err, result) {
      if (guild.me.hasPermission("ADMINISTRATOR")) {
        let embed = new Discord.RichEmbed()
        .setColor("RANDOM")
        .setDescription("**Initiating server request...**")
        await Original_Channel.send({embed})
        if (!guild.roles.find("name","Verified")) {
          guild.createRole({
            name: 'Verified',
            color: 'BLUE',
          })
          .then(async function(role) {
            let embed = new Discord.RichEmbed()
            .setColor("RANDOM")
            .setDescription("**Created Verified role**")
            await Original_Channel.send({embed})
          })
        }
      } else {
        Original_Channel.send("It seems I do not posses the **ADMINISTRATOR** ability..Would be nifty to have for better performance!")
        if (!guild.roles.find("name","Verified")) {
          Original_Channel.send("It seems you don't have a **Verified** role here either. Please make one!")
        }
        Original_Channel.send("**Initiating server request...**")
      }
      if (result){
        console.log(result)
        if (guild.me.hasPermission("ADMINISTRATOR")) {
          let embed = new Discord.RichEmbed()
          .setColor("RANDOM")
          .setDescription("**Previous data with this guild found**")
          await Original_Channel.send({embed})
          let embed2 = new Discord.RichEmbed()
          .setColor("RANDOM")
          .setDescription("**Do you wish to use the data found, or use default guild data?**")
          .addField("Valid Responses","**Default**\n**Previous**")
          .setFooter("Failure to respond in 90 seconds of this prompt, or invalid response will trigger default data creation")
          await Original_Channel.send({embed:embed2,reply: guild.owner})
          let collector = new Discord.MessageCollector(Original_Channel, m => m.author.id === guild.ownerID, { time: 90000, max: 1})
          collector.on('collect', async(message1) => {
            if (message1.content.toLowerCase() === "default") {
              let embed = new Discord.RichEmbed()
              .setColor("RANDOM")
              .setDescription("**Initiating default data creation..**")
              await Original_Channel.send({embed})
              var myobj = {
                  _id: guild.id,
                  groupId: 0,
                  logs: Original_Channel.name,
                  prefix: "-",
                  subgroups:[]
              };
              db.collection("Guild_Data").findOne({_id: guild.id}, async function(err, result) {
                if (result) {
                  db.collection("Guild_Data").deleteOne({_id: guild.id}, async function(err, obj) {
                    if (err) throw err
                    let embed = new Discord.RichEmbed()
                    .setColor("RANDOM")
                    .setDescription("**Deleted previous data, initiating default data creation..**")
                    await Original_Channel.send({embed})
                    var myobj = {
                      _id: guild.id,
                      groupId: 0,
                      logs: Original_Channel.name,
                      prefix: "-",
                      subgroups:[]
                    };
                    db.collection("Guild_Data").insertOne(myobj, async function(err, res) {
                      if (err) throw err;
                      let embed = new Discord.RichEmbed()
                      .setColor("RANDOM")
                      .setDescription(`Successfully initiated default guild data creation! You may now customize the settings available to you!\nThese settings are open to be edited using the command **${myobj.prefix}set** command!\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)
                      await Original_Channel.send({embed})
                    })
                  })
                }
                client.close()
              })
            } else if (message1.content.toLowerCase() === "previous") {
                let embed = new Discord.RichEmbed()
                .setColor("RANDOM")
                .setDescription("**Guild is now using previous data associated with it!**")
                await Original_Channel.send({embed})
            } else {
                let embed = new Discord.RichEmbed()
                embed.setColor(0xCC0000)
                embed.setDescription("**Invalid response received, initiating default data creation..**")
                embed.setTimestamp()
                await Original_Channel.send({embed})
                var myobj = {
                  _id: guild.id,
                  groupId: 0,
                  logs: Original_Channel.name,
                  prefix: "-",
                  subgroups:[]
              };
              var myquery = { _id: guild.id};
              db.collection("Guild_Data").deleteOne(myquery, function(err, obj) {
                db.collection("Guild_Data").insertOne(myobj, async function(err, res) {
                    if (err) throw err;
                    let embed2 = new Discord.RichEmbed()
                    .setColor("RANDOM")
                    .setDescription(`Successfully initiated default guild data creation! You may now customize the settings available to you!\nThese settings are open to be edited using the command **${myobj.prefix}set** command!\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)

                    await Original_Channel.send({embed2})
                })
                client.close()
              })
            }
          })
          collector.on('end', async(collection) => {
            if (collection.size === 0) {
                let embed = new Discord.RichEmbed()
                embed.setColor(0xCC0000)
                embed.setDescription("**Prompt exceeded timeout without response, initiating default data creation..**")
                await Original_Channel.send({embed})
                var myobj = {
                  _id: guild.id,
                  groupId: 0,
                  logs: Original_Channel.name,
                  prefix: "-",
                  subgroups:[]
              };
              db.collection("Guild_Data").insertOne(myobj, async function(err, res) {
                  if (err) throw err;
                  let embed2 = new Discord.RichEmbed()
                  .setColor("RANDOM")
                  .setDescription(`Successfully initiated default guild data creation! You may now customize the settings available to you!\nThese settings are open to be edited using the command **${myobj.prefix}set** command!\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)
                  .setTimestamp()
                  await Original_Channel.send({embed2})
                  client.close()
              })
                
            }
        })
        } else {
          Original_Channel.send("**Previous data with this guild found**")
          Original_Channel.send(`<@${guild.ownerID}> **Do you wish to use the data found, or use default guild data?**\n**Valid Responses**: **Default**, **Previous**`)
          let collector = new Discord.MessageCollector(Original_Channel, m => m.author.id === guild.ownerID, { time: 90000, max: 1})
          collector.on('collect', async(message1) => {
            if (message1.content.toLowerCase() === "default") {
              await Original_Channel.send("**Initiating default data creation..**")
              var myobj = {
                  _id: guild.id,
                  groupId: 0,
                  logs: Original_Channel.name,
                  prefix: "-",
                  subgroups:[]
              };
              db.collection("Guild_Data").findOne({_id: guild.id}, async function(err, result) {
                if (result) {
                  db.collection("Guild_Data").deleteOne({_id: guild.id}, async function(err, obj) {
                    if (err) throw err
                    await Original_Channel.send("**Deleted previous data, initiating default data creation..**")
                    var myobj = {
                      _id: guild.id,
                      groupId: 0,
                      logs: Original_Channel.name,
                      prefix: "-",
                      subgroups:[]
                    };
                    db.collection("Guild_Data").insertOne(myobj, async function(err, res) {
                      if (err) throw err
                      await Original_Channel.send(`Successfully initiated default guild data creation! You may now customize the settings available to you!\nThese settings are open to be edited using the command **${myobj.prefix}set** command!\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)
                    })
                    client.close()
                  })
                }
              })
            } else if (message1.content.toLowerCase() === "previous") {
                let embed = new Discord.RichEmbed()
                .setColor("RANDOM")
                .setDescription("**Guild is now using previous data associated with it!**")
                await Original_Channel.send({embed})
            } else {
                await Original_Channel.send("**Invalid response received, initiating default data creation..**")
                var myobj = {
                  _id: guild.id,
                  groupId: 0,
                  logs: Original_Channel.name,
                  prefix: "-",
                  subgroups:[]
              };
              var myquery = { _id: guild.id};
              db.collection("Guild_Data").deleteOne(myquery, function(err, obj) {
                db.collection("Guild_Data").insertOne(myobj, async function(err, res) {
                    if (err) throw err;
                    await Original_Channel.send(`Successfully initiated default guild data creation! You may now customize the settings available to you!\nThese settings are open to be edited using the command **${myobj.prefix}set** command!\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)
                })
                client.close()
              })
            }
          })
          collector.on('end', async(collection) => {
            if (collection.size === 0) {
                Original_Channel.send("**Prompt exceeded timeout without response, initiating default data creation..**")
                var myobj = {
                  _id: guild.id,
                  groupId: 0,
                  logs: Original_Channel.name,
                  prefix: "-",
                  subgroups:[]
              };
              db.collection("Guild_Data").deleteOne({_id: guild.id}, function(err, obj) {
                db.collection("Guild_Data").insertOne(myobj, async function(err, res) {
                    if (err) throw err;
                    Original_Channel.send(`Successfully initiated default guild data creation! You may now customize the settings available to you!\nThese settings are open to be edited using the command **${myobj.prefix}set** command!\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)
                })  
                client.close()
              })  
            }
        })
        }
      } else {
        if (guild.me.hasPermission("ADMINISTRATOR")) {
          let embed = new Discord.RichEmbed()
          .setColor("RANDOM")
          .setDescription("**No previous data with this guild found, initiating default data creation..**")
          await Original_Channel.send({embed})
        } else {
          Original_Channel.send("**No previous data with this guild found, initiating default data creation..**")
        }
        var myobj = {
          _id: guild.id,
          groupId: 0,
          logs: Original_Channel.name,
          prefix: "-",
          subgroups:[]
      };
      db.collection("Guild_Data").insertOne(myobj, async function(err, res) {
          if (err) throw err;
          if (guild.me.hasPermission("ADMINISTRATOR")) {
            let embed = new Discord.RichEmbed()
            embed.setColor(0x00AE86)
            embed.setDescription(`Successfully initiated default guild data creation! You may now customize the settings available to you!\nThese settings are open to be edited using the command **${myobj.prefix}set** command!\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)
            embed.setTimestamp()
            await Original_Channel.send({embed})
          } else {
            Original_Channel.send(`Successfully initiated default guild data creation! You may now customize the settings available to you!\nThese settings are open to be edited using the command **${myobj.prefix}set** command!\n**Prefix**\n**Logs**\n**Subgroups**\n**GroupId**`)
          }
          client.close()
      })
      }
    })
  })
})
bot.on("warn", console.warn)
bot.on("ready", function () {
  console.log(`MercApi: Ready to serve ${bot.users.size} users, in ${bot.channels.size} channels of ${bot.guilds.size} servers.`)
  let Master_Guild = bot.guilds.get('324336831300894721')
  let Exploit_Logger = Master_Guild.channels.find("name","exploit-logs")
  bot.user.setActivity('Prefix -', {
    type: 'WATCHING'
})
.then(presence => console.log(`Activity set to ${presence.game ? presence.game.name : 'none'}`))
.catch(console.error);
  app.get('/Exploiter/:userid/:placeid', async function (req, res, next) {
    var requiredFields = {
      'userid': 'int',
      'placeid': 'int'
    };
    var validate = [req.params];
    var opt = verifyParameters(res, validate, requiredFields);
    if (!opt) {
      return;
    }
    let Player_Info = await rbx2.getPlayerInfo(opt.userid)
    let embed = new Discord.RichEmbed()
    .setColor("RANDOM")
    .setTitle("Attempted PlaceStealer")
    .setThumbnail(Player_Info.Url)
    .setDescription(`**User**: ${Player_Info.username}\n**User_Profile**: https://www.roblox.com/users/${opt.userid}/profile\n**Place_Link**: https://www.roblox.com/games/${opt.placeid}`)
    .setFooter("Anti-PlaceSteal protection")
    await Exploit_Logger.send({embed})
  })
})
Rbx_Ready.on('begin',function () {
  bot.rbx.getCurrentUser().then(function(Data) {
    let GroupId = 3820064
    console.log(Data.UserName,"logged in!")
    console.log("Beginning member assesment..")
    let blacklist = [1, 261]
    var evt = bot.rbx.onJoinRequestHandle(GroupId)
    evt.on('data', function (request) {
      bot.rbx.getIdFromUsername(request.username).then(function (id) {
        for (var i = 0; i < blacklist.length; i++) {
          if (blacklist[i] === id) {
            evt.emit('handle', request, false);
            return;
          }
        }
        evt.emit('handle', request, true, function () {
          try { bot.rbx.message(id, 'Welcome', 'Welcome to my group') }
          catch (err) { console.log("Couldn't message this user", id) }
          return
        });
      });
    });
  })
})
setInterval(function() {
  http.get("http://testingmercbot.herokuapp.com");
}, 300000);